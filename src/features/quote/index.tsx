import * as React from 'react';
import { title } from './data.fixture';

export const Quote: React.FC = () => (
  <>
    <h1>{title}</h1>
  </>
);

export default Quote;
