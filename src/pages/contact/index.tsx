import * as React from 'react';
import { title } from './data.fixture';

export const contact: React.FC = (): React.ReactElement => (
  <>
    <h1>{title}</h1>
  </>
);

export default contact;
