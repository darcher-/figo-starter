import * as React from 'react';
import { title } from './data.fixture';

export const approot: React.FC = (): React.ReactElement => (
  <>
    <h1>{title}</h1>
    {/* TODO: use inline instead of url loader */}
  </>
);

export default approot;
