import { CLSReportCallback, ReportCallback } from 'web-vitals';

export const reportWebVitals = async <T = ReportCallback | CLSReportCallback>(
  onPerfEntry?: T,
): Promise<void> => {
  const { getCLS, getFID, getFCP, getLCP, getTTFB } = (await import('web-vitals')) || {};
  getCLS(onPerfEntry as CLSReportCallback);

  const ope = onPerfEntry as ReportCallback;
  getCLS(ope);
  getFID(ope);
  getFCP(ope);
  getLCP(ope);
  getTTFB(ope);
};

export default reportWebVitals;
