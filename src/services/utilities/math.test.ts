import { add } from './math.services';

it('renders page title', () => {
  expect(add(2, 3)).toEqual(5);
});
