module.export = {
  '*.(js|mjs)': 'eslint --fix',
  '*.(svg|html|md|json)}': 'prettier --write',
  '*.css': 'stylelint --fix',
};
